package com.industree.app.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R

class BookingHistoryAdapter : RecyclerView.Adapter<BookingHistoryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.booking_history_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {}
    override fun getItemCount(): Int {
        return 10
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var bookingHistoryImage: ImageView
        var bookingHistoryImage1: ImageView
        var namebookingHistory: TextView
        var distancebookingHistory: TextView
        var addressbookingHistory: TextView
        var cardView: CardView
        var textbookingHistory:TextView


        init {
            bookingHistoryImage = itemView.findViewById(R.id.bookingHistoryIV)
            bookingHistoryImage1 = itemView.findViewById(R.id.bookingHistoryIV1)
            namebookingHistory = itemView.findViewById(R.id.namebookingHistoryTV)
            distancebookingHistory = itemView.findViewById(R.id.distancebookingHistoryTV)
            addressbookingHistory = itemView.findViewById(R.id.addressbookingHistoryTV)
            textbookingHistory = itemView.findViewById(R.id.textbookingHistoryTV)
            cardView = itemView.findViewById(R.id.bookingHistoryCV)

        }
    }


}