package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.CreateRoomModel
import com.industree.app.main.Model.NotificationDataItem
import com.industree.app.main.Model.NotificationModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.NotificationClickListener
import com.industree.app.main.adapter.NotificationListAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : BaseActivity() {
    @BindView(R.id.backNotificationIV)
    lateinit var backNotificationIV: ImageView

    @BindView(R.id.notificationRV)
    lateinit var notificationRV: RecyclerView

    var notificationListAdapter: NotificationListAdapter? = null
    var NotifiList: ArrayList<NotificationDataItem?>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        ButterKnife.bind(mActivity)

        if (isNetworkAvailable(mActivity!!)) {
            executeNotificationDataRequest()
        } else {
            showToast(mActivity!!, getString(R.string.internet_connection_error))
        }

    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "10"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeNotificationDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.NotificationDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<NotificationModel> {
            override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<NotificationModel>,
                response: Response<NotificationModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mNotificationModel = response.body()!!
                Log.e("Log", mNotificationModel.status.toString())
                if (mNotificationModel.status == 1) {
                    NotifiList= mNotificationModel.data
                        setAdapter()
                }else{
                     showToast(mActivity,mNotificationModel.message)
                }
            }
        })
    }

    private fun setAdapter() {
        val layoutManager2: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        notificationRV.setLayoutManager(layoutManager2)
        notificationListAdapter = NotificationListAdapter(mActivity,NotifiList,mNotificationClickListener)
        notificationRV.setAdapter(notificationListAdapter)
    }
    var mNotificationClickListener : NotificationClickListener = object : NotificationClickListener {
        override fun onItemClickListener(
            notificationType: String?,
            otherUserId: String?,
            other_userName: String?,
            roomId: String?,
            userName1: String?
        ) {
            if(notificationType =="1"){
                var mIntent : Intent = Intent(mActivity, ChatActivity::class.java)
                mIntent.putExtra("room_id",roomId)
                mIntent.putExtra("other_user_name",userName1)
                startActivity(mIntent)
            }
            if(notificationType =="2"){
                var mIntent : Intent = Intent(mActivity, HomeActivity::class.java)
                mIntent.putExtra("Notification_type","1")
                startActivity(mIntent)
            }
            if(notificationType =="3"){
                var mIntent : Intent = Intent(mActivity, HomeActivity::class.java)
                mIntent.putExtra("Notification_type","1")
                startActivity(mIntent)
            }
        }

    }
    private fun executeCreateRoomApi(otherUserId: String?, other_userName: String?) {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_user_id"] = otherUserId
        mMap["vendor_id"] = ""
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.createRoomIdRequest(mHeaders,mMap)
        call.enqueue(object : Callback<CreateRoomModel> {
            override fun onFailure(call: Call<CreateRoomModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<CreateRoomModel>, response: Response<CreateRoomModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mCreateRoomModel = response.body()!!
                if (mCreateRoomModel!!.status == 1) {
                    var mIntent : Intent = Intent(mActivity, ChatActivity::class.java)
                    mIntent.putExtra("room_id",mCreateRoomModel.data!!.roomId)
                    mIntent.putExtra("other_user_name",other_userName)
                    startActivity(mIntent)
                } else {
                    showToast(mActivity,mCreateRoomModel?.message)
                }
            }
        })
    }
    @OnClick(
        R.id.backNotificationIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backNotificationIV -> onBackPressed()
        }
    }
}