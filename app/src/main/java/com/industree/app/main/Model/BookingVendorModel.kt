package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookingVendorModel(

	@field:SerializedName("data")
	val data: BookingData? = null,

	@field:SerializedName("services")
	val services: List<BookingServicesItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class BookingServicesItem(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("service_quantity")
	val serviceQuantity: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("service_id")
	val serviceId: String? = null,

	@field:SerializedName("id")
	val id: String? = null
) : Parcelable

@Parcelize
data class BookingData(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: String? = null,

	@field:SerializedName("end_time")
	val endTime: String? = null,

	@field:SerializedName("book_date")
	val bookDate: String? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: String? = null,

	@field:SerializedName("session_end")
	val sessionEnd: String? = null
) : Parcelable
