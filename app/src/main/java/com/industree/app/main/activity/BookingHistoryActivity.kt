package com.industree.app.main.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.industree.app.R
import com.industree.app.main.fragment.*

class BookingHistoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_history)
        var bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView1)
        switchFragment(BookingHistoryFragment())
        bottomNavigationView.setItemIconTintList(null)
        bottomNavigationView.setSelectedItemId(R.id.actionSubscription);
        bottomNavigationView.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.actionHome -> switchFragment(HomeFragment())
                R.id.actionFavourite -> switchFragment(FavouriteFragment())
                R.id.actionSubscription -> {
                    switchFragment(BookingHistoryFragment())
                }

                R.id.actionChat -> switchFragment(ChatFragment())
                R.id.actionProfile -> switchFragment(ProfileFragment())
            }
            true
        })
    }

    private fun switchFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.mainFL1, fragment, fragment.toString())
        fragmentTransaction.addToBackStack(fragment.toString())
        fragmentTransaction.commit()
    }
}
