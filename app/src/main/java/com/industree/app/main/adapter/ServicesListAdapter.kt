package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R
import com.industree.app.main.Model.ServicesItem
import com.industree.app.main.`interface`.BookingClickListener
import kotlinx.android.synthetic.main.service_list_item.view.*
import java.util.ArrayList

class ServicesListAdapter(
    var mActivity: Activity,
    var servicesArrayList: ArrayList<ServicesItem?>?,
    var mBookingClickListener: BookingClickListener
): RecyclerView.Adapter<ServicesListAdapter.MyViewHolder>()  {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ServicesListAdapter.MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.service_list_item, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ServicesListAdapter.MyViewHolder, position: Int) {
         holder.hairCutTV.setText(servicesArrayList!!.get(position)?.serviceName)
         holder.txtPriceTV.setText("$"+servicesArrayList?.get(position)?.servicePrice)
         holder.txtDetailTV.setText(servicesArrayList!!.get(position)?.serviceDescription)

        holder.txtMinusTV.setOnClickListener {
            val str:String = holder.txtCountTV.text as String
            val minus = str.toInt()
            if(minus > 0) {
                holder.txtCountTV.text = (minus - 1).toString()
            }
            val price= (servicesArrayList?.get(position)?.servicePrice)!!.toInt()
            val count =(holder.txtCountTV.text as String).toInt()
            val total=price*count
            mBookingClickListener.onItemClickListener(total,position,servicesArrayList?.size!!,count,servicesArrayList!!.get(position)?.serviceId!!.toInt())
        }
        holder.txtPlusTV.setOnClickListener {
            val str:String = holder.txtCountTV.text as String
            val plus = str.toInt()
            holder.txtCountTV.text= (plus+1).toString()
            val price= (servicesArrayList?.get(position)?.servicePrice)!!.toInt()
            val count =(holder.txtCountTV.text as String).toInt()
            val total=price*count
            mBookingClickListener.onItemClickListener(
                total,
                position,
                servicesArrayList?.size!!,
                count,
                servicesArrayList!!.get(position)?.serviceId!!.toInt()
            )
        }
    }

    override fun getItemCount(): Int {
        return servicesArrayList?.size!!
    }
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val hairCutTV:TextView=itemView.hairCutTV
        val txtDetailTV: TextView=itemView.txtDetailTV
        val txtCountTV : TextView=itemView.txtCountTV
        val txtPriceTV : TextView=itemView.txtPriceTV
        var txtMinusTV : TextView=itemView.txtMinusTV
        var txtPlusTV : TextView=itemView.txtPlusTV
    }

}