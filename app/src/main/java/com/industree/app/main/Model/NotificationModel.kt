package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationModel(

	@field:SerializedName("lastPage")
	val lastPage: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<NotificationDataItem?>? = null,

	@field:SerializedName("count")
	val count: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class NotificationDataItem(

	@field:SerializedName("room_id")
	val roomId: String? = null,

	@field:SerializedName("notification_type")
	val notificationType: String? = null,

	@field:SerializedName("notification_read_status")
	val notificationReadStatus: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("other_user_id")
	val otherUserId: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("notification_id")
	val notificationId: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("detail_id")
	val detailId: String? = null
) : Parcelable
