package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavouriteUnfavouriteModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("isFav")
	val isFav: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable
