package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R
import com.industree.app.main.Model.vendorServicesItem

class ServicesAdapter(var mActivity: Activity,var  services: ArrayList<vendorServicesItem?>?): RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServicesAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.service_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ServicesAdapter.ViewHolder, position: Int) {
          holder.serviceNameTV.setText(services!!.get(position)!!.serviceName)
        holder.txtPriceTV.setText("$"+services!!.get(position)!!.servicePrice)
        holder.txtDetailTV.setText(services!!.get(position)!!.serviceDescription)
    }

    override fun getItemCount(): Int {
       return services!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var serviceNameTV: TextView
            var txtPriceTV: TextView
            var txtDetailTV : TextView

        init {
            serviceNameTV=itemView.findViewById(R.id.serviceNameTV)
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV)
            txtDetailTV = itemView.findViewById(R.id.txtDetailTV)
        }
    }
}