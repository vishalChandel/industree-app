package com.industree.app.main.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.AddBusinessModel
import com.industree.app.main.Model.VendorCategoryItem
import com.industree.app.main.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubscriptionSecondActivity : BaseActivity() {
    @BindView(R.id.subscriptionSubmitTV)
    lateinit var subscriptionSubmitTV: TextView

    @BindView(R.id.vendorNameET)
    lateinit var vendorNameET: EditText

    @BindView(R.id.emailSubscriptionET)
    lateinit var emailSubscriptionET: EditText

    @BindView(R.id.phoneNumberSubscriptionET)
    lateinit var phoneNumberSubscriptionET: EditText

    @BindView(R.id.descriptionSubscriptionET)
    lateinit var descriptionSubscriptionET: EditText

    @BindView(R.id.websiteSubscriptionET)
    lateinit var websiteSubscriptionET: EditText

    @BindView(R.id.instaSubscriptionET)
    lateinit var instaSubscriptionET: EditText

    @BindView(R.id.facebookSubscriptionET)
    lateinit var facebookSubscriptionET: EditText

    @BindView(R.id.twitterSubscriptionET)
    lateinit var twitterSubscriptionET: EditText

    @BindView(R.id.youtubeSubscriptionET)
    lateinit var youtubeSubscriptionET: EditText

    @BindView(R.id.subsciptionIV2)
    lateinit var subsciptionIV2: ImageView

    var category: ArrayList<VendorCategoryItem?>? = ArrayList()
    var vendor_name:String?=null
    var email:String?=null
    var phone_no:String?=null
    var description:String?=null
    var website:String?=null
    var instagram:String?=null
    var facebook:String?=null
    var twiter:String?=null
    var youtube:String?=null
    var plan_id:String?=null
    var vendor_id:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_second)
        ButterKnife.bind(mActivity)
        category=intent.getParcelableArrayListExtra("category")
        vendor_name=intent.getStringExtra("vendor_name")
        email= intent.getStringExtra("email")
        plan_id=intent.getStringExtra("plan_id")
        phone_no=intent.getStringExtra("phone_num")
        description=intent.getStringExtra("description")
        website=intent.getStringExtra("website")
        instagram=intent.getStringExtra("instagrem")
        facebook= intent.getStringExtra("facebook")
        twiter=intent.getStringExtra("twiter")
        youtube=intent.getStringExtra("youtube")
        vendor_id=intent.getStringExtra("vendor_id")
        vendorNameET.setText(vendor_name)
        emailSubscriptionET.setText(email)
        phoneNumberSubscriptionET.setText(phone_no)
        descriptionSubscriptionET.setText(description)
        websiteSubscriptionET.setText(website)
        instaSubscriptionET.setText(instagram)
        facebookSubscriptionET.setText(facebook)
        twitterSubscriptionET.setText(twiter)
        youtubeSubscriptionET.setText(youtube)
    }
    @OnClick(
        R.id.subscriptionSubmitTV,R.id.subsciptionIV2
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subsciptionIV2 -> onBackPressed()
            R.id.subscriptionSubmitTV -> onSubmitClick()
        }
    }

    private fun onSubmitClick() {
        if (isNetworkAvailable(mActivity!!)) {
            executeSubmitDataRequest()
        } else {
            showToast(mActivity!!, getString(R.string.internet_connection_error))
        }
//        val mainIntent = Intent(mActivity, BookingHistoryActivity::class.java)
//        mainIntent.putExtra("abc", 123)
//        startActivity(mainIntent)
    }
    private fun mParam(): MutableMap<String?, Any?> {
        val mMap: MutableMap<String?, Any?> = java.util.HashMap()
        mMap["vendor_id"] = vendor_id
        mMap["user_id"] = getLoggedInUserID()
        mMap["plan_id"] = plan_id
        mMap["website"] = website
        mMap["youtube"] = youtube
        mMap["twitter"] = twiter
        mMap["instagram"] = instagram
        mMap["facebook"] = facebook
        mMap["phone_no"] = phone_no
        mMap["email"] = email
        mMap["vendor_name"]= vendor_name
       mMap["category"] = category
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSubmitDataRequest() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddBusinessDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<AddBusinessModel> {
            override fun onFailure(call: Call<AddBusinessModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<AddBusinessModel>, response: Response<AddBusinessModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
               var  mAddBusinessModel=  response.body()!!
                if (mAddBusinessModel!!.status == 1) {

                }else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

}