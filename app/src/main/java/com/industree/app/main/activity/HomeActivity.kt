package com.industree.app.main.activity

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.fragment.*
import com.industree.app.main.utils.*
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {
    @BindView(R.id.imgHomeIV)
    lateinit var actionHome: ImageView

    @BindView(R.id.imgFavouriteIV)
    lateinit var actionExplore: ImageView

    @BindView(R.id.imgSubscriptionIV)
    lateinit var actionNotification: ImageView

    @BindView(R.id.imgChatIV)
    lateinit var actionChat: ImageView

    @BindView(R.id.imgProfileIV)
    lateinit var actionProfile: ImageView

    var notification_type:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(mActivity)

        notification_type=intent.getStringExtra("Notification_type")
        if(notification_type != null)
        {
            performSubscriptionClick()
        }
        else{
            setUpFirstFragment()
        }


    }
    @OnClick(
        R.id.imgHomeIV,
        R.id.imgFavouriteIV,
        R.id.imgSubscriptionIV,
        R.id.imgChatIV,
        R.id.imgProfileIV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.imgHomeIV -> performHomeClick()
            R.id.imgFavouriteIV -> performExploreClick()
            R.id.imgSubscriptionIV ->performSubscriptionClick()
            R.id.imgChatIV -> performChatClick()
            R.id.imgProfileIV -> performProfileClick()

        }
    }
    private fun setUpFirstFragment() {
            performHomeClick()
    }
    private fun performHomeClick() {
        tabSelection(0)
        switchFragment(HomeFragment(), HOME_TAG, false, null)
    }
    private fun performExploreClick() {
        tabSelection(1)
        switchFragment(FavouriteFragment(), FAVOURITE_TAG, false, null)
    }
    private fun performSubscriptionClick() {
        tabSelection(2)
      //  switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
        switchFragment(BookingServicesFragment(), SUBSCRIPTION_TAG ,false,null )
    }
    private fun performChatClick() {
        tabSelection(3)
        switchFragment(ChatFragment(), CHAT_TAG,false , null)
    }
    private fun performProfileClick() {
        tabSelection(4)
        switchFragment(ProfileFragment() , PROFILE_TAG , false , null)
    }
    private fun tabSelection(mPos: Int) {
        imgHomeIV.setImageResource(R.drawable.home_icon)
        imgFavouriteIV.setImageResource(R.drawable.fav_icon)
        imgSubscriptionIV.setImageResource(R.drawable.subscription_icon)
        imgChatIV.setImageResource(R.drawable.chat_icon)
        imgProfileIV.setImageResource(R.drawable.user_icon)
        when(mPos){
            0 ->{
                imgHomeIV.setImageResource(R.drawable.ic_home_sel)
            }
            1 ->{
                imgFavouriteIV.setImageResource(R.drawable.ic_fav_sel)
            }
            2 ->{
                imgSubscriptionIV.setImageResource(R.drawable.ic_subscription_sel)
            }
            3 ->{
                imgChatIV.setImageResource(R.drawable.ic_chat_sel)
            }
            4 ->{
                imgProfileIV.setImageResource(R.drawable.ic_user_sel)
            }
        }
    }
}