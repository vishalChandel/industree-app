package com.industree.app.main.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import com.industree.app.R
import com.industree.app.main.Model.CreateRoomModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.fcm.CustomNotiModel
import com.industree.app.main.utils.SPLASH_TIME_OUT
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class SplashActivity : BaseActivity() {
    @BindView(R.id.splashIV)
    lateinit var splash: ImageView
    var mNotificationModel: CustomNotiModel? = null
    var notificationId: String? = ""
    var roomId: String? = ""
    var notificationType: String? = ""
    var notificationReadStatus: String? = ""
    var userId: String? = ""
    var otherUserID: String? = ""
    var username: String? = ""
    var created: String? = ""
    var description:String?=""
    var profileImage: String? = ""
    var title:String?=""
    var detail_id:String?=""
    private val SPLASH_DISPLAY_LENGTH = 3000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setUpSplash()
        getHashKey()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                if (isUserLogin()) {
                    if(isUserLogin()){
                        if (intent != null && intent.extras != null){
                            for (key in intent.extras!!.keySet()) {
                                val value = intent.extras!![key]
                                Log.d(TAG, "Key: $key Value: $value")
                                if (key.equals("room_id", ignoreCase = true)) {
                                    roomId = value.toString()
                                }
                                if (key.equals("notification_type", ignoreCase = true)) {
                                    notificationType = value.toString()
                                }
                                if (key.equals("notification_read_status", ignoreCase = true)) {
                                    notificationReadStatus = value.toString()
                                }
                                if (key.equals("user_id", ignoreCase = true)) {
                                    userId = value.toString()
                                }
                                if (key.equals("other_user_id", ignoreCase = true)) {
                                    otherUserID = value.toString()
                                }
                                if (key.equals("user_name", ignoreCase = true)) {
                                    username = value.toString()
                                }
                                if (key.equals("created", ignoreCase = true)) {
                                    created = value.toString()
                                }
                                if (key.equals("description", ignoreCase = true)) {
                                    description = value.toString()
                                }
                                if (key.equals("notification_id", ignoreCase = true)) {
                                    notificationId = value.toString()
                                }
                                if (key.equals("profileImage", ignoreCase = true)) {
                                    profileImage = value.toString()
                                }

                                if (key.equals("title", ignoreCase = true)) {
                                    title = value.toString()
                                }

                                if (key.equals("detail_id", ignoreCase = true)) {
                                    detail_id = value.toString()
                                }

                            }
                            mNotificationModel = CustomNotiModel(roomId,notificationType,notificationReadStatus,userId,otherUserID,username,created,description,notificationId,profileImage,title,detail_id)
                             if (mNotificationModel?.notificationType.equals("1")) {
                                 var mIntent : Intent = Intent(mActivity, ChatActivity::class.java)
                                 mIntent.putExtra("room_id",mNotificationModel!!.roomId)
                                 mIntent.putExtra("other_user_name",mNotificationModel!!.userName)
                                finish()
                            }else if (mNotificationModel?.notificationType.equals("2")) {
                                 var mIntent : Intent = Intent(mActivity, HomeActivity::class.java)
                                 mIntent.putExtra("Notification_type","1")
                                 startActivity(mIntent)
                                finish()
                            }else if (mNotificationModel?.notificationType.equals("3")) {
                                 var mIntent : Intent = Intent(mActivity, HomeActivity::class.java)
                                 mIntent.putExtra("Notification_type","1")
                                 startActivity(mIntent)
                                finish()
                            }else{
                                val i = Intent(mActivity, HomeActivity::class.java)
                                startActivity(i)
                                finish()
                            }
                        }
                            else{
                                  val i = Intent(mActivity, HomeActivity::class.java)
                                  startActivity(i)
                                  finish()
                            }
                    }
                    else{
                        val i = Intent(mActivity, SigninActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                }
                else{
                    val i = Intent(mActivity, SigninActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }
    // - - Code to print out the key hash
    open fun getHashKey() {
        try {
            val info = packageManager.getPackageInfo(
                "com.industree.app",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }

}

