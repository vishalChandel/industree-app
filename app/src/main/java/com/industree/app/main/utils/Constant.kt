package com.industree.app.main.utils

// - - Constant Values
const val SPLASH_TIME_OUT = 1500
//LIVE
//const val BASE_URL = "http://industree.online/webservices/"
//Staging
const val BASE_URL = "http://industree.online/webservices/"

const val PRIVACY_POLICY ="http://industree.online/webservices/privacyPolicy.html"
const val TERMS_OF_USE ="http://industree.online/webservices/TermsAndConditions.html"
/*
* Data Transfer
* */
const val DEVICE_TYPE = "2"
const val NOTI_TYPE = "noti_type"
/*
* Shared Preferences Keys
* */
const val IS_LOGIN = "is_login"
const val USERID = "user_id"
const val FIRSTNAME = "firstname"
const val LASTNAME="lastname"
const val EMAIL = "email"
const val PASSWORD ="password"
const val PROFILEPIC = "photo"

const val LATITUDE ="latitude"
const val LONGITUDE ="logitude"
const val AUTHTOKEN = "authToken"
const val PHONENO="phoneNo"

// - - Fragment Tags
const val HOME_TAG = "home_tag"
const val FAVOURITE_TAG="favourite_tag"
const val SUBSCRIPTION_TAG = "subscription_tag"
const val CHAT_TAG = "message_tag"
const val PROFILE_TAG = "profile_tag"

const val ALL_BOOKING_TAG = "all_booking_tag"
const val ON_GOING_BOOKING_TAG ="on_going_booking_tag"
const val COMPLETE_BOOKING_TAG ="complete_booking_tag"

const val SOCKET_URL = "http://jaohar-uk.herokuapp.com:80"
const val DEVICE_TOKEN="device_token"
const val GOOGLE_SIGN_IN= 123


/*
     *uf
     * Subscriptions Keys & Details
     * */
const val MERCHANT_ID: String = ""
const val LICENCE_KEY: String = ""
const val SUBSCRIPTION_PLAN_KEY: String = ""

const val IS_SUBSCRIPTION = "is_subscriptions"
