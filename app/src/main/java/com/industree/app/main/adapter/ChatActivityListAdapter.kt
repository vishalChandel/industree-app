package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.ChatMessageData
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ChatActivityListAdapter(
    var mActivity: Activity,
    var mMsgArrayList: ArrayList<ChatMessageData?>?,
    var loggedInUserID: String
) : RecyclerView.Adapter<ChatActivityListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.chat_activity_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(mMsgArrayList!!.get(position)!!.userId.equals(loggedInUserID)){
           holder.right_holder.visibility=View.VISIBLE
            holder.left_holder.visibility=View.GONE
            holder.senderTV.setText(mMsgArrayList!!.get(position)!!.message)
            val Timestamp: Long = mMsgArrayList?.get(position)!!.created!!.toLong()
            val timeD = Date(Timestamp * 1000)
            val sdf = SimpleDateFormat("MMM dd, hh:mm aa")
            sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
            val Time: String = sdf.format(timeD)
            holder.senderTimeTV.setText(Time)
        }
        else{
            holder.right_holder.visibility=View.GONE
            holder.left_holder.visibility=View.VISIBLE
            holder.receiverTV.setText(mMsgArrayList!!.get(position)!!.message)
            val Timestamp: Long = mMsgArrayList?.get(position)!!.created!!.toLong()
            val timeD = Date(Timestamp * 1000)
            val sdf = SimpleDateFormat("MMM dd, hh:mm aa")
            sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
            val Time: String = sdf.format(timeD)
            holder.recieverTimeTV.setText(Time)
            Glide.with(mActivity)
                .load(mMsgArrayList!!.get(position)!!.profileImage)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .centerCrop()
                .into(  holder.RevieverIV)
        }
    }
    override fun getItemCount(): Int {
        return mMsgArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var RevieverIV: ImageView
        var recieverTimeTV: TextView
        var chatActivityListLL:LinearLayout
        var senderMessage: TextView
        var senderTimeTV: TextView
        var receiverMessage: TextView
        var left_holder: RelativeLayout
         var right_holder:RelativeLayout
         var senderTV:TextView
         var receiverTV:TextView
        init {
            chatActivityListLL = itemView.findViewById(R.id.chatActivityListLL)
            RevieverIV = itemView.findViewById(R.id.RevieverIV)
            recieverTimeTV = itemView.findViewById(R.id.recieverTimeTV)
            senderMessage = itemView.findViewById(R.id.senderTV)
            senderTimeTV = itemView.findViewById(R.id.senderTimeTV)
            receiverMessage = itemView.findViewById(R.id.receiverTV)
            left_holder=itemView.findViewById(R.id.left_holder)
            right_holder=itemView.findViewById(R.id.right_holder)
            senderTV=itemView.findViewById(R.id.senderTV)
            receiverTV=itemView.findViewById(R.id.receiverTV)
        }
    }


}