package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.NotificationDataItem
import com.industree.app.main.`interface`.NotificationClickListener
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NotificationListAdapter(
    var mActivity: Activity,
    var NotifiList: ArrayList<NotificationDataItem?>?,
    var mNotificationClickListener: NotificationClickListener
) : RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.notification_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel = NotifiList!!.get(position)
        holder.textNotificationTV.text=NotifiList!!.get(position)!!.description

        val Timestamp: Long = NotifiList?.get(position)!!.created!!.toLong()
        val timeD = Date(Timestamp * 1000)
        val sdf = SimpleDateFormat("MMM dd, hh:mm aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
        val Time: String = sdf.format(timeD)
        holder.dateNotificationTV.text=Time

        Glide.with(mActivity!!)
            .load(NotifiList?.get(position)?.profileImage)
            .centerCrop()
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(holder.imageNotificationIV)

        holder.itemView.setOnClickListener {
            mNotificationClickListener.onItemClickListener(mModel!!.notificationType,mModel.otherUserId,mModel.userName,mModel.roomId,mModel.userName)

        }
    }
    override fun getItemCount(): Int {
        return NotifiList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageNotificationIV: ImageView
        var textNotificationTV: TextView
        var dateNotificationTV: TextView

        init {

            imageNotificationIV = itemView.findViewById(R.id.imageNotificationIV)
            textNotificationTV = itemView.findViewById(R.id.textNotificationTV)
            dateNotificationTV = itemView.findViewById(R.id.dateNotificationTV)

        }
    }

}