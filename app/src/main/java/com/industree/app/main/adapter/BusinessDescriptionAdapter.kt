package com.industree.app.main.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.DataItemList
import com.industree.app.main.Model.VendorCategoryItem
import com.industree.app.main.Model.vendorServicesItem
import com.industree.app.main.`interface`.AddServiceClickListener
import kotlin.collections.ArrayList

class BusinessDescriptionAdapter(
    var mActivity: Activity,
    var category: ArrayList<VendorCategoryItem?>?,
    var loggedInUserID: String,
    var authToken: String,
    var CategoryList: ArrayList<DataItemList?>?,
    var mAddServiceClickListener: AddServiceClickListener
) : RecyclerView.Adapter<BusinessDescriptionAdapter.ViewHolder>() {
    var services: ArrayList<vendorServicesItem?>? = ArrayList()
    var servicesTEmp: ArrayList<vendorServicesItem?>? = ArrayList()
    var businessServicesAdapter: BusinessServicesAdapter? = null
    var categoryName: ArrayList<String?>?= ArrayList()
    lateinit var adapter: ArrayAdapter<String>
    var id:ArrayList<String?>? = ArrayList()

    var temp:ArrayList<String?>? = ArrayList()

    var listTemp:ArrayList<ArrayList<vendorServicesItem>>?= ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessDescriptionAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.business_description_list, parent, false)
        return ViewHolder(view)
    }
    fun <T> hasDuplicates(arr: Array<T>): Boolean {
        return arr.size != hashSetOf(*arr).size
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: BusinessDescriptionAdapter.ViewHolder, position: Int) {
//        temp!!.add(category!!.get(position)!!.categoryId)
//        val distinct = temp!!.toSet().toList()
//
//
//        for (i in 0 until category!!.size)
//        {
//            for (j in i + 1 until category!!.size)
//            {
//                Log.e("Test!!", "onBindViewHolder: "+category!!.get(i)!!.services )
//
//                servicesTEmp=(category!!.get(i)!!.services)
//                if(category!!.get(i)!!.categoryName==category!!.get(j)!!.categoryName)
//                {
//
//                    servicesTEmp=(category!!.get(j)!!.services)
//                    listTemp!!.add(servicesTEmp)
//
//                }
//
//
//            }
//
//            Log.e("TempService***", "onBindViewHolder: "+servicesTEmp, )
//
//        }
//
//        Log.e("LISt***", "onBindViewHolder: "+distinct, )
//        for(item in distinct!!)
//        {
//
//            if(item.equals(category!!.get(position)!!.categoryId)){
//
//
//            }
//        }


 // set Spinner Category
        for(i in 0..(CategoryList!!.size-1)){
            //    categoryName!!.add(i,CategoryList!!.get(i)!!.title)
            categoryName!!.add(CategoryList!!.get(i)!!.title)
        }
        adapter = ArrayAdapter(mActivity, R.layout.spinner_item_selected, categoryName!!)
        holder.spinner.adapter = adapter
        holder.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                //         Toast.makeText(mActivity, parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        val selectionPosition = adapter.getPosition(category!!.get(position)!!.categoryName)
        holder.spinner.setSelection(selectionPosition)
        holder.brandNameET.setText(category!!.get(position)!!.brandName)
        holder.brandNameET.setOnClickListener {
            showBrandNamePopUp( holder.brandNameET.text.toString(),position)
        }
        holder.locationET.setText(category!!.get(position)!!.location)
        holder.locationET.setOnClickListener {
            showLocationPopUp( holder.locationET.text.toString(),position)
        }
        Glide.with(mActivity!!)
            .load(category?.get(position)?.brandImage)
            .centerCrop()
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .into(holder.ManageBusinessProfileIV)
        services = category!!.get(position)!!.services

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        holder.servicesRV.setLayoutManager(layoutManager)
        businessServicesAdapter = BusinessServicesAdapter(mActivity!!, services,authToken)
        holder.servicesRV.setAdapter(businessServicesAdapter)

        holder.addServiceTV.setOnClickListener {
            mAddServiceClickListener.onItemClickListener(services,position)
        }
    }

    private fun showBrandNamePopUp(toString: String, position: Int) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.edit_category_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var categoryET = alertDialog.findViewById<EditText>(R.id.categoryET)
          categoryET.setText(toString)
        val editTV =alertDialog.findViewById<TextView>(R.id.editTV)
        editTV.setOnClickListener {
            category!!.get(position)!!.brandName = categoryET.text.toString()
            notifyItemChanged(position)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }
    private fun showLocationPopUp(toString: String, position: Int) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.edit_category_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var categoryET = alertDialog.findViewById<EditText>(R.id.categoryET)
        categoryET.setText(toString)
        val editTV =alertDialog.findViewById<TextView>(R.id.editTV)
        editTV.setOnClickListener {
            category!!.get(position)!!.location = categoryET.text.toString()
            notifyItemChanged(position)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    override fun getItemCount(): Int {
        return category!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ManageBusinessProfileIV: ImageView
        var spinner : Spinner
        var brandNameET : TextView
        var locationET : TextView
        var servicesRV : RecyclerView
        var addServiceTV: TextView
        init {
            ManageBusinessProfileIV = itemView.findViewById(R.id.ManageBusinessProfileIV)
            spinner = itemView.findViewById(R.id.spinner)
            brandNameET = itemView.findViewById(R.id.brandNameET)
            locationET = itemView.findViewById(R.id.locationET)
            servicesRV = itemView.findViewById(R.id.servicesRV)
            addServiceTV = itemView.findViewById(R.id.addServiceTV)
        }
    }

}