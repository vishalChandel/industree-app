package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.industree.app.R
import com.industree.app.main.Model.ChatDataItem
import com.industree.app.main.Model.CreateRoomModel
import com.industree.app.main.Model.GetAllUserChatModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.ChatItemClickListner
import com.industree.app.main.activity.ChatActivity
import com.industree.app.main.adapter.ChatListAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ChatFragment : BaseFragment() {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder
    @BindView(R.id.chatRV)
    lateinit var chatRV: RecyclerView

    var chatListAdapter: ChatListAdapter? = null
    var ChatDetailList: ArrayList<ChatDataItem?>? = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_chat, container, false)
        mUnbinder = ButterKnife.bind(this, view)
    //    initRecyclerView(view)
        return view
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onResume() {
        super.onResume()
        if(isNetworkAvailable(activity!!)){
            getChatUsersRequest()
        }
        else {
            showToast(activity!!, getString(R.string.internet_connection_error))
        }
    }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "30"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun getChatUsersRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.getChatUsersDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<GetAllUserChatModel> {
            override fun onFailure(call: Call<GetAllUserChatModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetAllUserChatModel>, response: Response<GetAllUserChatModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mGetChatUsersModel : GetAllUserChatModel = response.body()!!

                if (mGetChatUsersModel!!.status == 1) {
                //    (activity as HomeActivity?)?.badgeChatCount(mGetChatUsersModel.unreadMessageCount!!.toInt())
                    ChatDetailList= mGetChatUsersModel.data
                    setAdapter()
                } else if (mGetChatUsersModel!!.status == 2) {
                    showAlertDialog(activity, mGetChatUsersModel.message)
                    // showDoubleButtonAlertDialog(mActivity, mSignInModel.message, editEmailET.text.toString())
                } else if (mGetChatUsersModel!!.status == 0) {
                    //    showAlertDialog(activity, mGetChatUsersModel.message)

                } else {
                    showAlertDialog(activity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        chatRV.setLayoutManager(layoutManager)
        chatListAdapter = ChatListAdapter(activity,ChatDetailList,mChatItemClickListner)
        chatRV.setAdapter(chatListAdapter)
    }

    var mChatItemClickListner : ChatItemClickListner = object : ChatItemClickListner {
        override fun onItemClickListner(mModel: ChatDataItem?) {
            if (isNetworkAvailable(activity!!)){
                executeCreateRoomApi(mModel,mModel!!.name)
//            val intent = Intent(activity, ChatActivity::class.java)
//            startActivity(intent)
            }else{
                showToast(activity,getString(R.string.internet_connection_error))
            }
        }

    }
    private fun executeCreateRoomApi(mModel: ChatDataItem?, name: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_user_id"] = mModel?.userId
        mMap["vendor_id"] = ""
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.createRoomIdRequest(mHeaders,mMap)
        call.enqueue(object : Callback<CreateRoomModel> {
            override fun onFailure(call: Call<CreateRoomModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<CreateRoomModel>, response: Response<CreateRoomModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mCreateRoomModel = response.body()!!
                if (mCreateRoomModel!!.status == 1) {

                    var mIntent : Intent = Intent(activity, ChatActivity::class.java)
                    mIntent.putExtra("room_id",mCreateRoomModel.data!!.roomId)
                    mIntent.putExtra("other_user_name",name)
                    startActivity(mIntent)
                } else {
                    showToast(activity,mCreateRoomModel?.message)
                }
            }
        })
    }
}