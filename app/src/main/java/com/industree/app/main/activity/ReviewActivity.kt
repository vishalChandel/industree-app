package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.RatingDetailModel
import com.industree.app.main.Model.ReviewDataItem
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.ReviewAdapter
import com.industree.app.main.adapter.ReviewListAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewActivity : BaseActivity() {
    @BindView(R.id.reviewRV)
    lateinit var reviewRV: RecyclerView

    @BindView(R.id.backRL)
    lateinit var backRL: RelativeLayout

    var reviewAdapter: ReviewAdapter? = null
    var vendor_id: String? = null
    var brand_id: String? = null
    var user_id: String? = null
    var reviewList: ArrayList<ReviewDataItem?>? = ArrayList()
    var reviewListAdapter: ReviewListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        ButterKnife.bind(mActivity)

        user_id = intent.getStringExtra("user_id")
        vendor_id = intent.getStringExtra("vendor_id")
        brand_id = intent.getStringExtra("brand_id")

        if (isNetworkAvailable(mActivity!!)){
            executeReviewRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }
    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()

        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = user_id
        mMap["vendor_id"] = vendor_id
        mMap["brand_id"] = brand_id
        mMap["pageNo"] = "1"
        mMap["perPage"] = "10"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeReviewRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.RatingDetailRequest(mHeaders,mParam())
        call.enqueue(object : Callback<RatingDetailModel> {
            override fun onFailure(call: Call<RatingDetailModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<RatingDetailModel>,
                response: Response<RatingDetailModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
               var  mRatingDetailModel = response.body()!!
                if (mRatingDetailModel.status == 1) {
                    reviewList=mRatingDetailModel.data
                    setAdapter()
                }
            }
        })
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        reviewRV.layoutManager = layoutManager
        reviewListAdapter = ReviewListAdapter(mActivity, reviewList)
        reviewRV.adapter = reviewListAdapter
    }
}