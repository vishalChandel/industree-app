package com.industree.app.main.`interface`

interface BookingClickListener {
    fun onItemClickListener(total: Int, position: Int, size: Int, count: Int, serviceId: Int)
}