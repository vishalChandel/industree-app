package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.industree.app.R
import com.industree.app.main.activity.PrivacyPolicy
import com.industree.app.main.activity.SubscriptionFirstActivity
import com.industree.app.main.activity.TermsOfUse
import com.industree.app.main.utils.*


class SubscriptionFragment : BaseFragment(), BillingProcessor.IBillingHandler {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.subscriptionCV1)
    lateinit var subscriptionCV1: CardView

    @BindView(R.id.subscriptionCV2)
    lateinit var subscriptionCV2: CardView

    @BindView(R.id.subscriptionCV3)
    lateinit var subscriptionCV3: CardView

    @BindView(R.id.termsConditionsTV)
    lateinit var termsConditionsTV: TextView

    @BindView(R.id.privacyPolicyTV)
    lateinit var privacyPolicyTV: TextView

    var mBillingProcessor: BillingProcessor? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_subscription, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        initializeSubscription()
        return view
    }
    @OnClick(
        R.id.subscriptionCV1,R.id.subscriptionCV2,R.id.subscriptionCV3,R.id.termsConditionsTV,R.id.privacyPolicyTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subscriptionCV1 -> onFirstSubscriptionClick()
            R.id.subscriptionCV2 -> onFirstSubscriptionClick()
            R.id.subscriptionCV3 -> onFirstSubscriptionClick()
            R.id.termsConditionsTV -> onClickTermsOfUse()
            R.id.privacyPolicyTV -> onClickPrivacyPolicy()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onClickPrivacyPolicy() {
        val i = Intent(activity!!, PrivacyPolicy::class.java)
        startActivity(i)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onClickTermsOfUse() {
        val i = Intent(activity!!, TermsOfUse::class.java)
        startActivity(i)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onFirstSubscriptionClick() {
        //        val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
//        startActivity(intent)

        //Actual
        if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
            subscriptionCallBack()
        } else {
            showToast(activity!!,getString(R.string.subscription_update_not))
        }

        //For Testing
        AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, true)
        showToast(activity,getString(R.string.purchase_subscription_))
       // activity!!.finish()
    }

    /*
    * In App  Subscriptions
    * */
    @SuppressLint("UseRequireInsteadOfGet")
    open fun initializeSubscription() {
        mBillingProcessor = BillingProcessor(activity!!, LICENCE_KEY, this)
        mBillingProcessor!!.initialize()
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun subscriptionCallBack() {
        // With Testing  PayLoads for Product Purchase & Subscription
        //mBillingProcessor.purchase(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        //mBillingProcessor.subscribe(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        if (isNetworkAvailable(activity!!)) {
            val extraParams = Bundle()
            extraParams.putString("accountId", MERCHANT_ID)
            mBillingProcessor!!.subscribe(activity!!, SUBSCRIPTION_PLAN_KEY, null , extraParams)
        } else {
            showToast(activity!!, getString(R.string.internet_connection_error))
        }
    }
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onProductPurchased(productId: String, mTransactionDetails: TransactionDetails?) {
        Log.e(TAG, "Purchase DATA :- " + mTransactionDetails!!.purchaseInfo.purchaseData)
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        val mPurchaseData = mTransactionDetails!!.purchaseInfo.purchaseData
        var mSubscriptionPlanKey = mTransactionDetails!!.purchaseInfo.purchaseData.productId

        Log.e(TAG, "onProduct Purchased_Data: " + mTransactionDetails!!.purchaseInfo.purchaseData.toString())
        Log.e(TAG, "onProduct Purchased_OrderID: " + mPurchaseData!!.orderId)
        Log.e(TAG, "onProduct Purchased_Token: " + mPurchaseData!!.purchaseToken)
        Log.e(TAG, "onProduct Purchased_Time: " + mPurchaseData!!.purchaseTime)
        Log.e(TAG, "Subscriptions Plan Key: $mSubscriptionPlanKey")


        if (mTransactionDetails!!.purchaseInfo == null) {
            AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, false)
            showToast(activity,getString(R.string.some_things))
        } else {
            AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, true)
            showToast(activity,getString(R.string.purchase_subscription_))
        }

        activity!!.finish()
    }

    override fun onPurchaseHistoryRestored() {

    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
        /*
        * Called when some error occurred. See Constants class for more details
        *
        * Note - this includes handling the case where the user canceled the buy dialog:
        * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        */
        Log.e(TAG, "onBillingError: ")
        when (errorCode) {
            0 -> Log.e("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK")
            1 -> Log.e(
                "onBillingError",
                "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED"
            )
            3 -> Log.e(
                "onBillingError",
                "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE"
            )
            4 -> Log.e(
                "onBillingError",
                "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE"
            )
            5 -> Log.e(
                "onBillingError",
                "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR"
            )
            6 -> Log.e(
                "onBillingError",
                "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR"
            )
            7 -> Log.e(
                "onBillingError",
                "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED"
            )
            8 -> Log.e(
                "onBillingError",
                "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED"
            )
        }
    }

    override fun onBillingInitialized() {
        /*
        * Called when BillingProcessor was initialized and it's ready to purchase
        */
        Log.e(TAG, "onBillingInitialized: ");
    }

    override fun onDestroy() {
        if (mBillingProcessor != null) mBillingProcessor!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mBillingProcessor!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

}