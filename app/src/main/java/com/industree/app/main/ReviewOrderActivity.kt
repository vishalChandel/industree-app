package com.industree.app.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.activity.BaseActivity
import com.industree.app.main.adapter.BookedDetailServicesAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ReviewOrderActivity : BaseActivity() {
    @BindView(R.id.backBookedServicesIV)
    lateinit var back: ImageView

    @BindView(R.id.nameBookedServicesTV)
    lateinit var nameBookedServicesTV: TextView

    @BindView(R.id.addressBookedServicesTV)
    lateinit var addressBookedServicesTV: TextView

    @BindView(R.id.bookedServicesIV)
    lateinit var bookedServicesIV: ImageView

    @BindView(R.id.seviceDetailRV)
    lateinit var seviceDetailRV: RecyclerView

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

    @BindView(R.id.bookedServicesSquareIV)
    lateinit var bookedServicesSquareIV: ImageView

    @BindView(R.id.bookedServicesCV)
    lateinit var bookedServicesCV: CardView

    @BindView(R.id.bookedServicesSquareCV)
    lateinit var bookedServicesSquareCV: CardView

    @BindView(R.id.distanceBookedServicesTV)
    lateinit var distanceBookedServicesTV: TextView


    var user_id:String?=null
    var booking_id:String?=null
    var type:String?=null
    var brand_name:String?=null
    var brand_image:String?=null
    var address:String?=null
    var email:String? = null
    var bookingServiceDetailList: ArrayList<BookingServiceDetail?>? = ArrayList()
    var bookedDetailServicesAdapter: BookedDetailServicesAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_order)
        ButterKnife.bind(mActivity)
        getIntentData()
        if(isNetworkAvailable(mActivity)){
            executeBookingServiceDetail()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }


        private fun mParam(): MutableMap<String?, String?> {
            val mMap: MutableMap<String?, String?> = HashMap()
            mMap["user_id"] = user_id
            mMap["booking_id"] = booking_id
            mMap["type"]=type
            Log.e(TAG, "**PARAM**$mMap")
            return mMap
        }
        private fun executeBookingServiceDetail() {
            val mHeaders: MutableMap<String, String> = HashMap()
            mHeaders["Token"] = getAuthToken()
            showProgressDialog(mActivity)
            val call = RetrofitClient.apiInterface.BookingServiceDetailRequest(mHeaders,mParam())
            call.enqueue(object : Callback<BookingServiceDetailModel> {
                override fun onFailure(call: Call<BookingServiceDetailModel>, t: Throwable) {
                    Log.e(TAG, t.message.toString())
                    dismissProgressDialog()
                }
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(
                    call: Call<BookingServiceDetailModel>,
                    response: Response<BookingServiceDetailModel>
                ) {
                    Log.e(TAG, response.body().toString())
                    dismissProgressDialog()
                    var mBookingServiceDetailModel = response.body()!!
                    if (mBookingServiceDetailModel.status == 1) {
                        nameBookedServicesTV.text=brand_name

                        if(mBookingServiceDetailModel.data!!.get(0)!!.brandImage.equals("") || mBookingServiceDetailModel.data!!.get(0)!!.brandImage.equals(null)){
                            bookedServicesCV.visibility=View.VISIBLE
                            bookedServicesSquareCV.visibility=View.GONE
                            Glide.with(mActivity!!)
                                .load(mBookingServiceDetailModel.data!!.get(0)!!.brandImage)
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder)
                                .into(bookedServicesIV)
                        }
                        else {
                            bookedServicesCV.visibility=View.GONE
                            bookedServicesSquareCV.visibility=View.VISIBLE
                            Glide.with(mActivity!!)
                                .load(mBookingServiceDetailModel.data!!.get(0)!!.brandImage)
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder)
                                .into(bookedServicesSquareIV)
                        }
                        bookingServiceDetailList=mBookingServiceDetailModel.data
                        txtPriceTV.text="$"+mBookingServiceDetailModel.totalAmount
                        if(type.equals("1")){
                            addressBookedServicesTV.text=email
                            bookedServicesCV.visibility=View.VISIBLE
                            bookedServicesSquareCV.visibility=View.GONE
                            Glide.with(mActivity!!)
                                .load(R.drawable.ic_placeholder)
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder)
                                .into(bookedServicesIV)
                        }
                        else if(type.equals("2")){
                            addressBookedServicesTV.text=address

                        }
                        if(mBookingServiceDetailModel.sessionEnd.equals("0")){
                            distanceBookedServicesTV.text="Booked"
                        }
                        else if(mBookingServiceDetailModel.sessionEnd.equals("3"))
                            distanceBookedServicesTV.text="Completed"
                        setAdapter()
                    }else{
                        showToast(mActivity,mBookingServiceDetailModel.message)
                    }

                }
            })
        }


    private fun getIntentData() {
        user_id=intent.getStringExtra("user_id")
        booking_id=intent.getStringExtra("booking_id")
        type=intent.getStringExtra("type")
        brand_name=intent.getStringExtra("brand_name")
        brand_image=intent.getStringExtra("brand_image")
        address=intent.getStringExtra("address")
        email=intent.getStringExtra("email")
    }

    @OnClick(
        R.id.backBookedServicesIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backBookedServicesIV -> onBackPressed()
        }
    }
    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        seviceDetailRV.setLayoutManager(layoutManager)
        bookedDetailServicesAdapter = BookedDetailServicesAdapter(mActivity,bookingServiceDetailList)
        seviceDetailRV.isNestedScrollingEnabled=false
        seviceDetailRV.setHasFixedSize(false)
        seviceDetailRV.setAdapter(bookedDetailServicesAdapter)
    }

}