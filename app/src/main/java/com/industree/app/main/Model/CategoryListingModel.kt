package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryListingModel(

	@field:SerializedName("data")
	val data: ArrayList<DataItemList?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class DataItemList(

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("catImage")
	val catImage: String? = null
) : Parcelable
