package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.*
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.AddServiceClickListener
import com.industree.app.main.adapter.BusinessDescriptionAdapter
import com.industree.app.main.adapter.ServicesListAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubscriptionFirstActivity : BaseActivity() {

    @BindView(R.id.continueSubscritionTV)
    lateinit var continueSubscritionTV: TextView

    @BindView(R.id.business_detailRV)
    lateinit var business_detailRV: RecyclerView

    //    @BindView(R.id.spinner)
//    lateinit var spinner: Spinner
    var CategoryList: ArrayList<DataItemList?>? = ArrayList()
    var categoryName: ArrayList<String?>? = ArrayList()
    var category: ArrayList<VendorCategoryItem?>? = ArrayList()
    lateinit var adapter: ArrayAdapter<String>
    var businessDescriptionAdapter: BusinessDescriptionAdapter? = null
    lateinit var mGetAllVendorByUserIdModel: GetAllVendorByUserIdModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_first)
        ButterKnife.bind(mActivity)
        if (isNetworkAvailable(mActivity!!)) {
            executeCategoryListingDataRequest()
            executeGetAllVendorDataRequest()
        } else {
            showToast(mActivity!!, getString(R.string.internet_connection_error))
        }
    }

    @OnClick(
        R.id.continueSubscritionTV, R.id.subsciptionIV1
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subsciptionIV1 -> onBackPressed()
            R.id.continueSubscritionTV -> onCountinueClick()

        }
    }

    private fun onCountinueClick() {
        val intent = Intent(mActivity, SubscriptionSecondActivity::class.java)
        intent.putParcelableArrayListExtra("category", category)
        intent.putExtra("vendor_name", mGetAllVendorByUserIdModel.data!!.vendorName)
        intent.putExtra("email", mGetAllVendorByUserIdModel.data!!.email)
        intent.putExtra("phone_num", mGetAllVendorByUserIdModel.data!!.phoneNo)
        intent.putExtra("description", mGetAllVendorByUserIdModel.data!!.description)
        intent.putExtra("website", mGetAllVendorByUserIdModel.data!!.website)
        intent.putExtra("instagrem", mGetAllVendorByUserIdModel.data!!.instagram)
        intent.putExtra("facebook", mGetAllVendorByUserIdModel.data!!.facebook)
        intent.putExtra("twiter", mGetAllVendorByUserIdModel.data!!.twitter)
        intent.putExtra("youtube", mGetAllVendorByUserIdModel.data!!.youtube)
        intent.putExtra("plan_id", mGetAllVendorByUserIdModel.data!!.planId)
        intent.putExtra("vendor_id", mGetAllVendorByUserIdModel.data!!.vendorId)
        startActivity(intent)
    }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllVendorDataRequest() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.getAllVendorByUserIdDataRequest(mHeaders, mParam())
        call.enqueue(object : Callback<GetAllVendorByUserIdModel> {
            override fun onFailure(call: Call<GetAllVendorByUserIdModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<GetAllVendorByUserIdModel>,
                response: Response<GetAllVendorByUserIdModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mGetAllVendorByUserIdModel = response.body()!!
                if (mGetAllVendorByUserIdModel!!.status == 1) {
                    category = mGetAllVendorByUserIdModel.category
                    setAdapter()
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun setAdapter() {
            val layoutManager: RecyclerView.LayoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            business_detailRV.setLayoutManager(layoutManager)
            businessDescriptionAdapter = BusinessDescriptionAdapter(
                mActivity!!,
                category,
                getLoggedInUserID(),
                getAuthToken(),
                CategoryList,
                mAddServiceClickListener
            )
            business_detailRV.setAdapter(businessDescriptionAdapter)
        }
        var mAddServiceClickListener: AddServiceClickListener = object : AddServiceClickListener {
            override fun onItemClickListener(
                services: ArrayList<vendorServicesItem?>?,
                position: Int
            ) {
                addServiceAlertDialog(mActivity, services,position)

            }
        }

        fun addServiceAlertDialog(
            mActivity: Activity?,
            services: ArrayList<vendorServicesItem?>?,
            position: Int
        ) {
            val alertDialog = Dialog(mActivity!!)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.add_service_dialog)
            alertDialog.setCanceledOnTouchOutside(true)
            alertDialog.setCancelable(true)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            // set the custom dialog components - text, image and button
            val serviceNameET = alertDialog.findViewById<EditText>(R.id.serviceNameET)
            val serviceCostET = alertDialog.findViewById<EditText>(R.id.serviceCostET)
            val serviceDescriptionET = alertDialog.findViewById<EditText>(R.id.serviceDescriptionET)
            val submitTV = alertDialog.findViewById<TextView>(R.id.submitTV)
            submitTV.setOnClickListener {
                val service_name = serviceNameET.text.toString()
                val service_cost = serviceCostET.text.toString()
                val service_description = serviceDescriptionET.text.toString()
                if (service_name.equals("") || service_cost.equals("") || service_description.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.all_fiels_required))
                } else {
                   category!![position]!!.services!!.add(vendorServicesItem(service_description, "", service_cost, service_name, "", "", ""))
                    businessDescriptionAdapter!!.notifyDataSetChanged()
                    alertDialog.dismiss()
                }

            }
            alertDialog.show()
        }

        private fun mParams(): MutableMap<String?, String?> {
            val mMap: MutableMap<String?, String?> = HashMap()
            mMap["user_id"] = getLoggedInUserID()
            Log.e(TAG, "**PARAM**$mMap")
            return mMap
        }

        private fun executeCategoryListingDataRequest() {
            val mHeaders: MutableMap<String, String> = HashMap()
            mHeaders["Token"] = getAuthToken()
            //   showProgressDialog(activity)
            val call = RetrofitClient.apiInterface.categoryListingDataRequest(mHeaders, mParams())
            call.enqueue(object : Callback<CategoryListingModel> {
                override fun onFailure(call: Call<CategoryListingModel>, t: Throwable) {
                    Log.e(TAG, t.message.toString())
                    //            dismissProgressDialog()
                }

                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(
                    call: Call<CategoryListingModel>,
                    response: Response<CategoryListingModel>
                ) {
                    Log.e(TAG, response.body().toString())
                    //            dismissProgressDialog()
                    var mCategoryListingModel: CategoryListingModel = response.body()!!
                    if (mCategoryListingModel.status == 1) {
                        CategoryList = mCategoryListingModel.data
                        //      setSpinner()
                    } else {
                        showToast(mActivity, mCategoryListingModel.message)
                    }
                }
            })

        }

//    private fun setSpinner() {
//        for(i in 0..(CategoryList!!.size-1)){
//        //    categoryName!!.add(i,CategoryList!!.get(i)!!.title)
//            categoryName!!.add(CategoryList!!.get(i)!!.title)
//        }
//        adapter = ArrayAdapter(this, R.layout.spinner_item_selected, categoryName!!)
//        spinner.adapter = adapter
//        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>,
//                view: View,
//                position: Int,
//                id: Long
//            ) {
//       //         Toast.makeText(mActivity, parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show()
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {
//
//            }
//        }
//    }
    }