package com.industree.app.main.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Options(
    var option_key: String? = null,
    var option_value: String? = null,
) : Parcelable