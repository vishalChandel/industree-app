package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.DataItem
import com.industree.app.main.Model.HomeDataModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.HomeVerticalListAdapter
import kotlinx.android.synthetic.main.activity_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class SearchActivity : BaseActivity() {
    @BindView(R.id.searchET)
    lateinit var searchET: EditText

    @BindView(R.id.cancelTV)
    lateinit var cancelTV: TextView

    @BindView(R.id.searchRV)
    lateinit var searchRV: RecyclerView

    @BindView(R.id.txtMessage)
    lateinit var txtMessage: TextView

    var search_categoryId:String?= null
    lateinit var mHomeDataModel:HomeDataModel
    var homeDetailItemList: ArrayList<DataItem?>? = ArrayList()
    var homeVerticalListAdapter: HomeVerticalListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        ButterKnife.bind(mActivity)

        search_categoryId=intent.getStringExtra("category_id")
        if (isNetworkAvailable(mActivity!!)) {
            executeSearchDataRequest(search_categoryId)
        } else {
            showToast(mActivity!!, getString(R.string.internet_connection_error))
        }
        searchET.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS or
                InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES


        searchET.setOnEditorActionListener(TextView.OnEditorActionListener() { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                //do here your stuff f
                //recentSearchLL.setVisibility(View.GONE)
                if (isValidate()!!)
                    if (isNetworkAvailable(mActivity)) {
                    executeSearchDataRequest(search_categoryId)
                }
                else {
                    showToast(mActivity, getString(R.string.internet_connection_error))
                }

                //  hideKeyBoard(mActivity, getCurrentFocus())
                return@OnEditorActionListener true
            }
            false
        })
    }
    @OnClick(
        R.id.cancelTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.cancelTV -> finish()
        }
    }
    private fun isValidate(): Boolean {
        var flag = true
        when {
            searchET.text.toString().trim { it <= ' ' } == "" -> {
                txtMessage.visibility=View.GONE
                flag = false
            }
        }
        return flag
    }
    private fun mParam(categoryId: String?): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["latitude"] = "0.0"
        mMap["longitude"] = "0.0"
        mMap["search"]=searchET.text.toString()
        mMap["category_id"]=categoryId
        mMap["pageNo"]="1"
        mMap["perPage"]="100"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSearchDataRequest(categoryId: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.HomeDataRequest(mHeaders,mParam(categoryId))
        call.enqueue(object : Callback<HomeDataModel> {
            override fun onFailure(call: Call<HomeDataModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeDataModel>,
                response: Response<HomeDataModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mHomeDataModel = response.body()!!
                Log.e("Log", mHomeDataModel.status.toString())
                if (mHomeDataModel.status == 1) {
                    homeDetailItemList=mHomeDataModel.data
                    if(homeDetailItemList!!.size==0){
                        txtMessage.visibility=View.VISIBLE
                        searchRV.visibility=View.GONE
           //             showAlertDialog(mActivity,getString(R.string.no_vendor_around_you))
                    }
                    else{
                        txtMessage.visibility=View.GONE
                        searchRV.visibility=View.VISIBLE
                        setAdapter(categoryId)
                    }
                }else{
                    // showToast(activity,mHomeDataModel.message)
           //         showAlertDialog(mActivity,getString(R.string.no_vendor_around_you))
                    txtMessage.visibility=View.VISIBLE
                    searchRV.visibility=View.GONE
                }
            }
        })
    }

    private fun setAdapter(categoryId: String?) {
        //    Vertical List
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        searchRV.setLayoutManager(layoutManager)
        homeVerticalListAdapter = HomeVerticalListAdapter(mActivity,homeDetailItemList,categoryId,getLoggedInUserID())
        searchRV.setAdapter(homeVerticalListAdapter)
    }
}