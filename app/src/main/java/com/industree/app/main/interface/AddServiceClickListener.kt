package com.industree.app.main.`interface`

import com.industree.app.main.Model.vendorServicesItem

interface AddServiceClickListener {
    fun onItemClickListener(services: ArrayList<vendorServicesItem?>?, position: Int)
}