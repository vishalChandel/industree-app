package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class GetVendorDetailModel(

	@field:SerializedName("data")
	val data: VenderData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("category")
	val category: List<CategoryItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class VenderData(

	@field:SerializedName("youtube")
	val youtube: String? = null,

	@field:SerializedName("phone_no")
	val phoneNo: String? = null,

	@field:SerializedName("website")
	val website: String? = null,

	@field:SerializedName("distance")
	val distance: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("count")
	val count: String? = null,

	@field:SerializedName("vendor_name")
	val vendorName: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("instagram")
	val instagram: String? = null,

	@field:SerializedName("isFav")
	val isFav: String? = null,

	@field:SerializedName("twitter")
	val twitter: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("review")
	val review:  @RawValue Any? = null,

//	@field:SerializedName("review")
//	val review: ArrayList<ReviewItem?>? = ArrayList(),

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null
) : Parcelable

@Parcelize
data class ReviewItem(

	@field:SerializedName("rating_id")
	val ratingId: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("review")
	val review: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null
) : Parcelable

@Parcelize
data class CategoryItem(

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("service_price")
	val servicePrice: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("brand_name")
	val brandName: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("services")
	val services: ArrayList<ServicesItem?>? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null
) : Parcelable

@Parcelize
data class ServicesItem(

	@field:SerializedName("service_description")
	val serviceDescription: String? = null,

	@field:SerializedName("service_price")
	val servicePrice: String? = null,

	@field:SerializedName("service_name")
	val serviceName: String? = null,

	@field:SerializedName("service_id")
	val serviceId: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null
) : Parcelable



//package com.industree.app.main.Model
//
//import android.os.Parcelable
//import com.google.gson.annotations.SerializedName
//import kotlinx.android.parcel.Parcelize
//import kotlinx.android.parcel.RawValue
//
//@Parcelize
//data class GetVendorDetailModel(
//
//	@field:SerializedName("data")
//	val data: VendorData? = null,
//
//	@field:SerializedName("message")
//	val message: String? = null,
//
//	@field:SerializedName("category")
//	val category: ArrayList<CategoryItem?>? = null,
//
//	@field:SerializedName("status")
//	val status: Int? = null
//) : Parcelable
//
//@Parcelize
//data class CategoryItem(
//
//	@field:SerializedName("address")
//	val address: String? = null,
//
//	@field:SerializedName("category_name")
//	val categoryName: String? = null,
//
//	@field:SerializedName("category_id")
//	val categoryId: String? = null,
//
//	@field:SerializedName("service_price")
//	val servicePrice: String? = null,
//
//	@field:SerializedName("brand_image")
//	val brandImage: String? = null,
//
//	@field:SerializedName("vendor_id")
//	val vendorId: String? = null,
//
//	@field:SerializedName("brand_name")
//	val brandName: String? = null,
//
//	@field:SerializedName("location")
//	val location: String? = null,
//
//	@field:SerializedName("services")
//	val services: ArrayList<ServicesItem?>? = null,
//
//	@field:SerializedName("brand_id")
//	val brandId: String? = null
//) : Parcelable
//
//@Parcelize
//data class VendorData(
//
//	@field:SerializedName("youtube")
//	val youtube: String? = null,
//
//	@field:SerializedName("phone_no")
//	val phoneNo: String? = null,
//
//	@field:SerializedName("website")
//	val website: String? = null,
//
//	@field:SerializedName("distance")
//	val distance: String? = null,
//
//	@field:SerializedName("created")
//	val created: String? = null,
//
//	@field:SerializedName("facebook")
//	val facebook: String? = null,
//
//	@field:SerializedName("rating")
//	val rating: String? = null,
//
//	@field:SerializedName("count")
//	val count: String? = null,
//
//	@field:SerializedName("vendor_name")
//	val vendorName: String? = null,
//
//	@field:SerializedName("description")
//	val description: String? = null,
//
//	@field:SerializedName("instagram")
//	val instagram: String? = null,
//
//	@field:SerializedName("isFav")
//	val isFav: String? = null,
//
//	@field:SerializedName("twitter")
//	val twitter: String? = null,
//
//	@field:SerializedName("user_id")
//	val userId: String? = null,
//
//	@field:SerializedName("brand_image")
//	val brandImage: String? = null,
//
//	@field:SerializedName("review")
//	val review:  @RawValue Any? = null,
//
////	@field:SerializedName("review")
////	val review: ArrayList<ReviewItem?>? = null,
//
//	@field:SerializedName("vendor_id")
//	val vendorId: String? = null,
//
//	@field:SerializedName("email")
//	val email: String? = null,
//
//	@field:SerializedName("plan_id")
//	val planId: String? = null
//) : Parcelable
//
//@Parcelize
//data class ReviewItem(
//
//	@field:SerializedName("rating_id")
//	val ratingId: String? = null,
//
//	@field:SerializedName("user_id")
//	val userId: String? = null,
//
//	@field:SerializedName("review")
//	val review: String? = null,
//
//	@field:SerializedName("created")
//	val created: String? = null,
//
//	@field:SerializedName("vendor_id")
//	val vendorId: String? = null,
//
//	@field:SerializedName("rating")
//	val rating: String? = null,
//
//	@field:SerializedName("profileImage")
//	val profileImage: String? = null,
//
//	@field:SerializedName("first_name")
//	val firstName: String? = null,
//
//	@field:SerializedName("brand_id")
//	val brandId: String? = null
//) : Parcelable
//
//@Parcelize
//data class ServicesItem(
//
//	@field:SerializedName("service_description")
//	val serviceDescription: String? = null,
//
//	@field:SerializedName("service_price")
//	val servicePrice: String? = null,
//
//	@field:SerializedName("service_name")
//	val serviceName: String? = null,
//
//	@field:SerializedName("service_id")
//	val serviceId: String? = null,
//
//	@field:SerializedName("brand_id")
//	val brandId: String? = null
//) : Parcelable
