package com.industree.app.main.utils

import android.app.Application
import io.branch.referral.Branch
import io.branch.referral.BuildConfig
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException


class IndustreeApplication: Application() {
    /**
     * Getting the Current Class Name
     */
    val TAG: String = "IndustreeApp"

    /**
     * Initialize the Applications Instance
     */
    private var mInstance: IndustreeApplication? = null

    /**
     * Synchronized the Application Class Instance
     */
    @Synchronized
    open fun getInstance(): IndustreeApplication? {
        return mInstance
    }
    /*
    * Socket
    * */
    var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()
        mInstance = this

        //Set Socket
        try {
            mSocket = IO.socket(SOCKET_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

        //Set Branch ios
//        val branch = Branch.getAutoInstance(this)
//        if (BuildConfig.DEBUG) {
//            branch.setDebug()
//        }

    }

    fun getSocket(): Socket? {
        return mSocket
    }
}