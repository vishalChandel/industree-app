package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.GetVendorDetailModel
import com.industree.app.main.Model.HelpModel
import com.industree.app.main.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.roundToInt

class HelpActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.txtsaveTV)
    lateinit var txtsaveTV: TextView

    @BindView(R.id.messageET)
    lateinit var messageET: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        ButterKnife.bind(mActivity)
    }
    @OnClick(
        R.id.backRL,R.id.txtsaveTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.txtsaveTV -> onSaveClick()
        }
    }

    private fun onSaveClick() {
        if(messageET.text.toString() != "") {
            if (isNetworkAvailable(mActivity)) {
                executeHelpRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
        else{
            showAlertDialog(mActivity,getString(R.string.please_enter_message))
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["message"] = messageET.text.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeHelpRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.HelpRequest(mHeaders, mParam())
        call.enqueue(object : Callback<HelpModel> {
            override fun onFailure(call: Call<HelpModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HelpModel>,
                response: Response<HelpModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                 var mHelpModel = response.body()!!
                if (mHelpModel.status == 1) {
                    showHelpAlertDialog(mActivity,getString(R.string.your_request_sent_successfully_we_will_reach))
                } else {
                    showToast(mActivity, mHelpModel.message)
                }

            }
        })
    }
    /*
*
* Error Alert Dialog
* */
    fun showHelpAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
        messageET.setText("")
            finish()
        }
        alertDialog.show()
    }
}