package com.industree.app.main.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.industree.app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}