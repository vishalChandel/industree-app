package com.industree.app.main.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.`interface`.CategoryClickListener
import com.industree.app.main.Model.DataItemList
import java.util.ArrayList


class HomeHorizontalListAdapter(
    var activity: FragmentActivity?,
    var CategoryList: ArrayList<DataItemList?>?,
   var mChatItemClickListner: CategoryClickListener
) : RecyclerView.Adapter<HomeHorizontalListAdapter.ViewHolder>() {

    private var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.home_horizontal_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        var mModel  = CategoryList?.get(position)
        holder.mainLayout.setBackgroundResource(R.drawable.cardview_bg)
        holder.cardview.setBackgroundResource(R.drawable.circle_bg)
        holder.horizontalListText.setTextColor(Color.parseColor("#000000"))
        Glide.with(activity!!)
                .load(CategoryList?.get(position)?.catImage)
                .placeholder(R.drawable.ic_placeholder_image)
                .error(R.drawable.ic_placeholder_image)
                .into(holder.horizontalListImage)
            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        if (position == 0) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        } else if (position == 1) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        } else if (position == 2) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        } else if (position == 3) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }
//        else if (position == 4) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }
//        else if (position == 5) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }
//        else if (position == 6) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }
//        else if (position == 7) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }
//        else if (position == 8) {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }
//        else {
//            Glide.with(activity!!)
//                .load(CategoryList?.get(position)?.catImage)
//                .placeholder(R.drawable.ic_placeholder_image)
//                .error(R.drawable.ic_placeholder_image)
//                .into(holder.horizontalListImage)
//            holder.horizontalListText.setText(CategoryList?.get(position)!!.title)
//        }

        if (selectedItem == position) {
            holder.mainLayout.setBackgroundResource(R.drawable.cardview_bgg)
            holder.cardview.setBackgroundResource(R.drawable.circle_bgg)
            holder.horizontalListText.setTextColor(Color.parseColor("#937938"))
        }

        holder.mainLayout.setOnClickListener {
            val previousItem = selectedItem
            selectedItem = position
            notifyItemChanged(previousItem)
            notifyItemChanged(position)
            mChatItemClickListner.onItemClickListener(mModel)
        }
    }

    override fun getItemCount(): Int {
        return CategoryList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val linearLayout: LinearLayout
        var horizontalListImage: ImageView
        var horizontalListText: TextView
        var cardview: RelativeLayout
        var mainLayout: RelativeLayout

        init {
            horizontalListImage = itemView.findViewById(R.id.horizontalListIV)
            horizontalListText = itemView.findViewById(R.id.horizontalListTV)
            linearLayout = itemView.findViewById(R.id.parentLL)
            mainLayout = itemView.findViewById(R.id.mailLayout)
            cardview = itemView.findViewById(R.id.horizontalListCV)
        }
    }

}