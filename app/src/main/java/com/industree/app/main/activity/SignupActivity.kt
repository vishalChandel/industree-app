package com.industree.app.main.activity

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.EasyLocationProvider
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.internal.CallbackManagerImpl
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.industree.app.R
import com.industree.app.main.Model.FacebookLoginModel
import com.industree.app.main.Model.GoogleLoginModel
import com.industree.app.main.Model.SignUpModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.utils.*
import kotlinx.android.synthetic.main.activity_signin.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL
import java.util.*

class SignupActivity : BaseActivity() {
    @BindView(R.id.signUpFirstNameTV)
    lateinit var firstNameET: EditText

    @BindView(R.id.signUpLastNameTV)
    lateinit var lastNameET: EditText

    @BindView(R.id.signUpEmailTV)
    lateinit var emailET: EditText

    @BindView(R.id.signUpPasswordTV)
    lateinit var passwordET: EditText

    @BindView(R.id.imgAgreeIV)
    lateinit var imgAgreeCB: CheckBox

    @BindView(R.id.signupTV)
    lateinit var signupTV: TextView

    @BindView(R.id.txtSigninTV)
    lateinit var txtSigninTV: TextView

    @BindView(R.id.googleCV)
    lateinit var googleCV: CardView

    @BindView(R.id.facebookCV)
    lateinit var facebookCV: CardView

    @BindView(R.id.privacyPolicyTV)
    lateinit var privacyPolicyTV: TextView

    @BindView(R.id.termsConditionsTV)
    lateinit var termsConditionsTV: TextView
    var mDeviceToken: String = ""
    private var checkUncheck: Int = 0
    var latitude=0.0
    var longitude= 0.0
    var REQUEST_PERMISSION_CODE=325
    var isCurrentLocation=true
    var locationAddress=""
    var addressCount = 0
    var mAccessFineLocation =android.Manifest.permission.ACCESS_FINE_LOCATION
    var mAccessCourseLocation =android.Manifest.permission.ACCESS_COARSE_LOCATION
    var  easyLocationProvider: EasyLocationProvider?=null
    lateinit var  mGoogleSignInClient: GoogleSignInClient
    lateinit var mFirebaseAuth: FirebaseAuth
    var mGoogleUserId:String?=null
    var mGoogleEmail:String?=null
    var mGoogleProfileImage:String?=null
    var mGoogleFirstName:String?=null
    var mGoogleLastName:String?=null
    var facebookId:String?=null
    var fbFirstName:String?=null
    var fbLastName:String?=null
    var fbEmail:String?=null
    var lg_PhotoUrl:String?=null
    var fbToken:String? = null
    var fbUsername:String? = null
   lateinit var  callbackManager: CallbackManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        ButterKnife.bind(this)

        setEditTextFocused(passwordET)

        getDeviceToken()
        performLocationClick()
        initializeGoogle()
    }
    @OnClick(
        R.id.txtSigninTV,
        R.id.imgAgreeIV,
        R.id.signupTV,
        R.id.googleCV,
        R.id.facebookCV,
        R.id.privacyPolicyTV,
        R.id.termsConditionsTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSigninTV -> performLoginClick()
            R.id.imgAgreeIV -> performAgreeClick()
            R.id.signupTV -> performSignUpClick()
            R.id.googleCV -> onGoogleClick()
            R.id.facebookCV -> onFacebookClick()
            R.id.privacyPolicyTV -> onClickPrivacyPolicy()
            R.id.termsConditionsTV -> onClickTermsOfUse()
        }
    }

    private fun onClickTermsOfUse() {
        val i = Intent(mActivity, TermsOfUse::class.java)
        startActivity(i)
    }

    private fun onClickPrivacyPolicy() {
        val i = Intent(mActivity, PrivacyPolicy::class.java)
        startActivity(i)
    }

    private fun onFacebookClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            FacebookSdk.sdkInitialize(applicationContext)
            LoginManager.getInstance().logOut()
            AppEventsLogger.activateApp(application)
            callbackManager = CallbackManager.Factory.create()
            facebookLogin()
        }
    }
    private fun facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.e("onSuccess: ", loginResult.accessToken.token)
                getFacebookData(loginResult)
            }

            override fun onCancel() {
                Log.e("FacebookData", "onCancel" + "onCancel")
            }

            override fun onError(error: FacebookException) {
                Log.e("FacebookData", "FacebookException" + "FacebookException")
            }
        })
    }

    fun getFacebookData(loginResult: LoginResult) {
        showProgressDialog(mActivity)
        val graphRequest = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
            dismissProgressDialog()
            try {
                if (`object`!!.has("id")) {
                    facebookId = `object`.getString("id")
                }
                if (`object`.has("first_name")) {
                    fbFirstName = `object`.getString("first_name")
                }
                if (`object`.has("last_name")) {
                    fbLastName = `object`.getString("last_name")
                }
                if (`object`.has("email")) {
                    fbEmail = `object`.getString("email")
                }

                val jsonObject = JSONObject(`object`.getString("picture"))
                if (jsonObject != null) {
                    val dataObject = jsonObject.getJSONObject("data")
                    lg_PhotoUrl = URL("https://graph.facebook.com/" + facebookId.toString() + "/picture?width=500&height=500").toString()
                }
                fbToken = facebookId
                Log.e("fbToken: ", loginResult.accessToken.token)
                Log.e("token ", fbToken!!)
                fbUsername="$fbFirstName $fbLastName"
                val dataObject = jsonObject.getJSONObject("data")
                executeFacebookApi()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val bundle = Bundle()
        Log.e("SignUpActivity", "bundle set")
        bundle.putString("fields", "id,first_name,last_name,email,picture")
        graphRequest.parameters = bundle
        graphRequest.executeAsync()
    }
    private fun facebookParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["first_name"] = fbFirstName
        mMap["last_name"] = fbLastName
        mMap["email"]= fbEmail
        mMap["device_token"] = mDeviceToken
        mMap["device_type"] = "2"
        mMap["latitude"] = latitude.toString()
        mMap["longitude"] = longitude.toString()
        mMap["facebook_token"] = fbToken
        Log.e("FbParams", mMap.toString())
        return mMap
    }
    private fun executeFacebookApi() {
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.facebookLogInDataRequest(facebookParams())

        call.enqueue(object : Callback<FacebookLoginModel> {
            override fun onFailure(call: Call<FacebookLoginModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<FacebookLoginModel>, response: Response<FacebookLoginModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mFacebookLoginModel = response.body()
                if (mFacebookLoginModel!!.status == 1) {
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mFacebookLoginModel.data!!.userId)
                    AppPrefrences().writeString(mActivity, EMAIL, mFacebookLoginModel.data!!.email)
                    AppPrefrences().writeString(mActivity, FIRSTNAME, mFacebookLoginModel.data.firstName)
                    AppPrefrences().writeString(mActivity, LASTNAME, mFacebookLoginModel.data.lastName)
                    AppPrefrences().writeString(mActivity, PASSWORD, mFacebookLoginModel.data.password)
                    AppPrefrences().writeString(mActivity, PROFILEPIC,mFacebookLoginModel.data!!.profileImage)
                    AppPrefrences().writeString(mActivity, AUTHTOKEN, mFacebookLoginModel!!.data!!.authToken)
                    AppPrefrences().writeString(mActivity,PHONENO, mFacebookLoginModel!!.data!!.phoneNo)
                    AppPrefrences().writeString(mActivity, LATITUDE,"" +latitude)
                    AppPrefrences().writeString(mActivity, LONGITUDE, "" + longitude)

                    val i = Intent(mActivity, HomeActivity::class.java)
                    i.putExtra(NOTI_TYPE,"0")
                    startActivity(i)
                    finish()
                } else if (mFacebookLoginModel.status == 0) {
                    showAlertDialog(mActivity, mFacebookLoginModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun onGoogleClick() {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.google_alert_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnCancelTV = alertDialog.findViewById<TextView>(R.id.btnCancelTV)
        val btnContinueTV = alertDialog.findViewById<TextView>(R.id.btnContinueTV)
        val titleTV=alertDialog.findViewById<TextView>(R.id.titleTV)
        titleTV.text= "\"Industree theodeocampo \" Wants to Use \" google.com\" to Sign In"
        txtMessageTV.text = "This allows the app and website to share information about you."
        btnCancelTV.setOnClickListener {
            alertDialog.dismiss()
        }
        btnContinueTV.setOnClickListener {
            val signInIntent: Intent = mGoogleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
            alertDialog.dismiss()
        }
        alertDialog.show()

    }

    private fun initializeGoogle() {
        FirebaseApp.initializeApp(this)
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mFirebaseAuth = FirebaseAuth.getInstance()
        FirebaseAuth.getInstance().signOut()
        signOut()
    }

    private fun signOut() {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener(this) {
                Log.e(TAG, "==Logout Successfully==")
            }
    }
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode() && requestCode != null) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }else{
        when (requestCode) {
            GOOGLE_SIGN_IN -> {
                val completedTask: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(completedTask)
            }}
        }
    }
    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            mGoogleUserId = account.id.toString()
            Log.e("Token", account.idToken.toString())
            val name = account.displayName
            Log.e("ServerAuthCode", account.serverAuthCode.toString())
            mGoogleEmail = account.email.toString()
            mGoogleProfileImage = if (account.photoUrl != null) {
                account.photoUrl.toString()
            } else {
                ""
            }
            val idx = name!!.lastIndexOf(' ')
            if (idx == -1) {
                return
            }
            mGoogleFirstName = name.substring(0, idx)
            mGoogleLastName = name.substring(idx + 1)
            Log.i("mGoogleUserId", mGoogleUserId!!)
            executeGoogleLogInApi()
        } catch (e: ApiException) {
            Log.e("MyTAG", "signInResult:failed code=" + e.statusCode)
          //  showToast(mActivity, "Failed")
        }
    }
    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["first_name"] = mGoogleFirstName
        mMap["last_name"] = mGoogleLastName
        mMap["email"]= mGoogleEmail
        mMap["device_token"] = mDeviceToken
        mMap["device_type"] = "1"
        mMap["latitude"] = latitude.toString()
        mMap["longitude"] = longitude.toString()
        mMap["google_image"] = mGoogleProfileImage
        mMap["google_token"] = mGoogleUserId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeGoogleLogInApi() {
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.googleLogInDataRequest(mParams())

        call.enqueue(object : Callback<GoogleLoginModel> {
            override fun onFailure(call: Call<GoogleLoginModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<GoogleLoginModel>, response: Response<GoogleLoginModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mGoogleLoginModel = response.body()
                if (mGoogleLoginModel!!.status == 1) {
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mGoogleLoginModel.data!!.userId)
                    AppPrefrences().writeString(mActivity, EMAIL, mGoogleLoginModel.data!!.email)
                    AppPrefrences().writeString(mActivity, FIRSTNAME, mGoogleLoginModel.data.firstName)
                    AppPrefrences().writeString(mActivity, LASTNAME, mGoogleLoginModel.data.lastName)
                    AppPrefrences().writeString(mActivity, PASSWORD, mGoogleLoginModel.data.password)
                    AppPrefrences().writeString(mActivity, PROFILEPIC,mGoogleLoginModel.data!!.profileImage)
                    AppPrefrences().writeString(mActivity, AUTHTOKEN, mGoogleLoginModel!!.data!!.authToken)
                    AppPrefrences().writeString(mActivity,PHONENO, mGoogleLoginModel!!.data!!.phoneNo)
                    AppPrefrences().writeString(mActivity, LATITUDE,"" +latitude)
                    AppPrefrences().writeString(mActivity, LONGITUDE, "" + longitude)

                    val i = Intent(mActivity, HomeActivity::class.java)
                    i.putExtra(NOTI_TYPE,"0")
                    startActivity(i)
                    finish()
                } else if (mGoogleLoginModel.status == 0) {
                    showAlertDialog(mActivity, mGoogleLoginModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun performLocationClick() {
        if (checkLocationPermission()) {
            getLocationLatLong()
        } else {
            requestLocationPermission()
        }
    }
    fun getLocationLatLong() {
        easyLocationProvider = EasyLocationProvider.Builder(mActivity)
            .setInterval(5000)
            .setFastestInterval(3000)
            .setPriority(com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setListener(object : EasyLocationProvider.EasyLocationCallback {
                override fun onGoogleAPIClient(googleApiClient: GoogleApiClient?, message: String) {
                    Log.e("EasyLocationProvider", "onGoogleAPIClient: $message")
                }

                override fun onLocationUpdated(mlatitude: Double, mlongitude: Double) {
                    Log.e(TAG, "onLocationUpdated:: Latitude: $mlatitude Longitude: $mlongitude")
                    if (isCurrentLocation) {
                        //dynamically
                        isCurrentLocation = false
                        latitude = mlatitude
                        longitude = mlongitude
                        getUserAddressFromLatLong(latitude, longitude)
                    }
                }

                override fun onLocationUpdateRemoved() {
                    Log.e("EasyLocationProvider", "onLocationUpdateRemoved")
                }
            }).build()
        lifecycle.addObserver(easyLocationProvider!!)
    }
    fun getUserAddressFromLatLong(mlatitude: Double, mlongitude: Double) {
        val addresses: List<Address>
        val geocoder: Geocoder = Geocoder(mActivity, Locale.getDefault())
        addresses = geocoder.getFromLocation(
            mlatitude,
            mlongitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        if (addresses.size > 0) {
            val address: String = addresses[0].getAddressLine(0)
            val city: String = addresses[0].locality
            val state: String = addresses[0].adminArea
            val country: String = addresses[0].countryName
            val postalCode: String = addresses[0].postalCode
            val knownName: String = addresses[0].featureName
            val code: String = addresses[0].countryCode
            locationAddress = "$city, $country, $code"

            if (addressCount == 0) {
             //   editLocationdET.setText(locationAddress)
                addressCount = 1
            }
            Log.e(TAG, "$city,$country,$code")
        }
    }
    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, arrayOf(mAccessFineLocation, mAccessCourseLocation), REQUEST_PERMISSION_CODE)
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //    openCamera()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
            400 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //    pickImageFromGallery(dialog!!)
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //Perform Location Click Code
                    getLocationLatLong()
                }
            }
        }
    }
    private fun checkLocationPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }
    private fun getDeviceToken() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.e(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            mDeviceToken = task.result!!
            AppPrefrences().writeString(
                mActivity,
                DEVICE_TOKEN,
                mDeviceToken
            )
            Log.e(TAG, "getDeviceToken: " + mDeviceToken)
        })
    }
    private fun performSignUpClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSignUpRequest()
//                val i = Intent(mActivity, SignInActivity::class.java)
//                startActivity(i)
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["first_name"] = firstNameET.text.toString()
        mMap["last_name"] = lastNameET.text.toString()
        mMap["email"]=emailET.text.toString()
        mMap["password"] = passwordET.text.toString()
        mMap["device_token"] = mDeviceToken
        mMap["device_type"] = DEVICE_TYPE
        mMap["latitude"] = latitude.toString()
        mMap["longitude"] = longitude.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSignUpRequest() {
//        val mHeaders: MutableMap<String, String> = HashMap()
//        mHeaders["Token"] = getAuthToken()
         showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.signUpDataRequest(mParam())

        call.enqueue(object : Callback<SignUpModel> {
            override fun onFailure(call: Call<SignUpModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<SignUpModel>, response: Response<SignUpModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mSignUpModel = response.body()
                if (mSignUpModel!!.status == 1) {
                    showOpenActivityAlertDialog(mActivity, mSignUpModel.message)
                    firstNameET.setText("")
                    lastNameET.setText("")
                    emailET.setText("")
                    passwordET.setText("")
                } else if (mSignUpModel.status == 0) {
                    showAlertDialog(mActivity, mSignUpModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.you_have_not_verified_your_email_address_please_check_your_email))
                }
            }
        })
    }

    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            firstNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
                flag = false
            }
            lastNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
                flag = false
            }
            emailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmaillId(emailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            passwordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
            passwordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            checkUncheck == 0 -> {
                showAlertDialog(mActivity, getString(R.string.please_read_and_accent_terms_of_use_and_privacy_policy))
                flag = false
            }

        }
        return flag
    }
    private fun performAgreeClick() {
        if (checkUncheck == 0) {
            imgAgreeCB.setBackgroundResource(R.drawable.ic_check)
            checkUncheck++
        } else {
            imgAgreeCB.setBackgroundResource(R.drawable.ic_uncheck)
            checkUncheck--
        }
    }
    private fun performLoginClick() {
        startActivity(Intent(mActivity, SigninActivity::class.java))
        finish()
    }
}
